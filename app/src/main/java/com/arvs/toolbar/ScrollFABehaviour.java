package com.arvs.toolbar;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.NestedScrollView;
import android.util.AttributeSet;
import android.view.View;

public class ScrollFABehaviour extends FloatingActionButton.Behavior {

    public ScrollFABehaviour(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onStartNestedScroll(CoordinatorLayout coordinatorLayout,FloatingActionButton child,View directTargetChild, @NonNull View target, int axes, int type) {
        return axes==ViewCompat.SCROLL_AXIS_VERTICAL;


        //return super.onStartNestedScroll(coordinatorLayout, child, directTargetChild, target, axes, type);
    }

    @Override
    public void onNestedScroll(@NonNull CoordinatorLayout coordinatorLayout, @NonNull FloatingActionButton child, @NonNull View target, int dxConsumed, int dyConsumed, int dxUnconsumed, int dyUnconsumed, int type) {
      if(dyConsumed>0&&child.getVisibility()==View.VISIBLE)
      {
          child.hide();
      } else  if(dyConsumed<0&&child.getVisibility()==View.GONE)
      {
          child.show();
      }


       // super.onNestedScroll(coordinatorLayout, child, target, dxConsumed, dyConsumed, dxUnconsumed, dyUnconsumed, type);
    }
}
